<?php

namespace Acme\HelloBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;


class DefaultController extends Controller
{
    public function indexAction($name)
    {
      //return
      //$this->render('AcmeHelloBundle:Default:index.html.twig',
      //array('name' => $name));
          $response = new JsonResponse();
          $response->setData(array(
                                   'hello' => $name
                                   ));
          return $response;
    }
}
